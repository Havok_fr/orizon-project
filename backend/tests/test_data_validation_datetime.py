from datetime import datetime
from fastapi.exceptions import HTTPException
import pytest
from src.data_validation.datetime import assert_timezone_offset_is_valid


@pytest.mark.parametrize(
    "datetime_str,expected_response",
    [
        ("2000-01-01T12:00:01-13:00", "Timezone offset UTC-13:00 is not valid"),
        ("2000-01-01T12:00:01+15:00", "Timezone offset UTC+15:00 is not valid"),
        ("2000-01-01T12:00:01+05:01", "Timezone offset UTC+05:01 is not valid"),
    ],
)
def test_assert_timezone_offset_is_valid(datetime_str, expected_response):
    # given
    date_time = datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%S%z")

    # when
    with pytest.raises(HTTPException) as excinfo:
        assert_timezone_offset_is_valid(date_time)

    # then
    assert excinfo.value.status_code == 422
    assert excinfo.value.detail == expected_response
