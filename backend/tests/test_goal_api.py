import pytest
from httpx import AsyncClient
from src.models import Goal, GoalStatus, Step
from tests.conftest import assert_model_objects_are_equal

BASE_ROUTE = "/goals"

pytestmark = pytest.mark.asyncio


async def test_get_goals(async_client: AsyncClient):
    # when
    response = await async_client.get(BASE_ROUTE)

    # then
    assert response.status_code == 200
    goals = response.json()
    assert len(goals) == 1
    expected_goal_schema = {
        "id": 1,
        "status": "TODO",
        "title": "current user goal title",
    }
    assert goals[0] == expected_goal_schema


async def test_get_goal_by_id_ownGoal(
    async_client: AsyncClient,
    freeze_time,
):
    # given
    current_user_goal_id = 1

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{current_user_goal_id}")

    # then
    assert response.status_code == 200
    goal = response.json()
    utc_now = freeze_time
    expected_goal_schema = {
        "id": 1,
        "description": "current user goal description",
        "status": "TODO",
        "title": "current user goal title",
        "posts": [
            {
                "id": 1,
                "date": utc_now,
                "description": "current user post description",
                "number_of_comments_and_replies": 2,
                "number_of_likes": 0,
                "is_liked": False,
            }
        ],
        "steps": [
            {
                "id": 1,
                "completion_date": utc_now,
                "description": "current user step description",
            }
        ],
        "user": {
            "id": 1,
            "uid": "CurrentUserUid",
            "fullname": "current_user_firstname current_user_lastname",
            "profile_picture_url": "current_user_profile_picture_url",
        },
    }
    assert goal == expected_goal_schema


async def test_get_goal_by_id_followingGoal(
    async_client: AsyncClient,
    current_user_follow_other_user,
    freeze_time,
):
    # given
    other_user_goal_id = 2

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{other_user_goal_id}")

    # then
    assert response.status_code == 200
    goal = response.json()
    utc_now = freeze_time
    expected_goal_schema = {
        "id": 2,
        "description": "other user goal description",
        "status": "TODO",
        "title": "other user goal title",
        "posts": [
            {
                "id": 2,
                "date": utc_now,
                "description": "other user post description",
                "number_of_comments_and_replies": 2,
                "number_of_likes": 0,
                "is_liked": False,
            }
        ],
        "steps": [
            {
                "id": 2,
                "completion_date": utc_now,
                "description": "other user step description",
            }
        ],
        "user": {
            "id": 2,
            "uid": "OtherUserUid",
            "fullname": "other_user_firstname other_user_lastname",
            "profile_picture_url": "other_user_profile_picture_url",
        },
    }
    assert goal == expected_goal_schema


async def test_get_goal_by_id_withGoalIsPublic(
    async_client: AsyncClient,
    make_other_user_public,
    freeze_time,
):
    # given
    other_user_goal_id = 2

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{other_user_goal_id}")

    # then
    assert response.status_code == 200
    goal = response.json()
    utc_now = freeze_time
    expected_goal_schema = {
        "id": 2,
        "description": "other user goal description",
        "status": "TODO",
        "title": "other user goal title",
        "posts": [
            {
                "id": 2,
                "date": utc_now,
                "description": "other user post description",
                "number_of_comments_and_replies": 2,
                "number_of_likes": 0,
                "is_liked": False,
            }
        ],
        "steps": [
            {
                "id": 2,
                "completion_date": utc_now,
                "description": "other user step description",
            }
        ],
        "user": {
            "id": 2,
            "uid": "OtherUserUid",
            "fullname": "other_user_firstname other_user_lastname",
            "profile_picture_url": "other_user_profile_picture_url",
        },
    }
    assert goal == expected_goal_schema


async def test_get_goal_by_id_withAccessForbidden(async_client: AsyncClient):
    # given
    other_user_goal_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.get(
        f"{BASE_ROUTE}/{other_user_goal_id}",
    )

    # then
    assert response.status_code == 403
    assert response.json() == expected_response


async def test_get_goal_by_id_withIdDoesNotExist(async_client: AsyncClient):
    # given
    unexisting_goal_id = 99
    expected_response = {"detail": "Goal with id 99 does not exist"}

    # when
    response = await async_client.get(
        f"{BASE_ROUTE}/{unexisting_goal_id}",
    )

    # then
    assert response.status_code == 404
    assert response.json() == expected_response


async def test_search_goal(async_client: AsyncClient):
    # given
    goal_title = "current user goal"

    # when
    response = await async_client.get(f"{BASE_ROUTE}/search/{goal_title}")

    # then
    assert response.status_code == 200
    goals = response.json()
    assert len(goals) == 1
    expected_goal_schema = {
        "id": 1,
        "status": "TODO",
        "title": "current user goal title",
    }
    assert goals[0] == expected_goal_schema


async def test_search_goal_withOtherUserGoalTitle(
    async_client: AsyncClient,
):
    # given
    other_user_goal_title = "other user goal title"

    # when
    response = await async_client.get(f"{BASE_ROUTE}/search/{other_user_goal_title}")

    # then
    assert response.status_code == 200
    goals = response.json()
    assert len(goals) == 0


async def test_search_goal_withoutMatchingGoal(
    async_client: AsyncClient,
):
    # given
    inexisting_title = "inexisting_title"

    # when
    response = await async_client.get(f"{BASE_ROUTE}/search/{inexisting_title}")

    # then
    assert response.status_code == 200
    goals = response.json()
    assert len(goals) == 0


async def test_create_goal_without_step(
    async_client: AsyncClient,
    freeze_time,
):
    # given
    request_body = {
        "request_goal": {
            "description": "my description",
            "status": "DONE",
            "title": "title",
        }
    }

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 200
    goal = response.json()
    expected_goal_schema = {
        "id": 3,
        "status": "DONE",
        "title": "title",
    }
    assert goal == expected_goal_schema
    utc_now = freeze_time
    expected_object = Goal(
        title="title",
        description="my description",
        status=GoalStatus.DONE,
        creation_date=utc_now,
        user_id=1,
    )
    assert_model_objects_are_equal(await Goal.get(id=goal["id"]), expected_object)


async def test_create_goal_with_steps(
    async_client: AsyncClient,
    freeze_time,
):
    # given
    utc_now = freeze_time
    request_body = {
        "request_goal": {
            "description": "my description",
            "status": "DONE",
            "title": "title",
        },
        "request_steps": [
            {
                "description": "my description1",
                "completion_date": utc_now,
            },
            {
                "description": "my description2",
                "completion_date": utc_now,
            },
        ],
    }

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 200
    goal = response.json()
    expected_goal_schema = {
        "id": 3,
        "status": "DONE",
        "title": "title",
    }
    assert goal == expected_goal_schema
    expected_object = Goal(
        title="title",
        description="my description",
        status=GoalStatus.DONE,
        creation_date=utc_now,
        user_id=1,
    )
    assert_model_objects_are_equal(await Goal.get(id=goal["id"]), expected_object)

    steps = await Step.filter(goal_id=goal["id"])
    assert len(steps) == 2
    expected_step_object1 = Step(
        description="my description1",
        completion_date=utc_now,
        goal_id=goal["id"],
    )
    expected_step_object2 = Step(
        description="my description2",
        completion_date=utc_now,
        goal_id=goal["id"],
    )
    assert_model_objects_are_equal(steps[0], expected_step_object1)
    assert_model_objects_are_equal(steps[1], expected_step_object2)


async def test_create_goal_withInvalidGoalStatus(async_client: AsyncClient):
    # given
    request_body = {
        "request_goal": {
            "title": "title",
            "description": "my description",
            "status": "WRONG",
        }
    }

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 422
    assert response.json() == {"detail": "WRONG is not a possible value for its field"}


async def test_create_goal_withEmptyTitle(async_client: AsyncClient):
    # given
    request_body = {
        "request_goal": {
            "title": "",
            "description": "my description",
            "status": "TODO",
        }
    }

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 422
    assert response.json() == {"detail": "title field can not be empty"}


async def test_update_goal(
    async_client: AsyncClient,
    freeze_time,
):
    # given
    utc_now = freeze_time
    current_user_goal_id = 1
    request_body = {
        "request_goal": {
            "description": "my description",
            "status": "DONE",
            "title": "my title",
        },
        "request_steps": [
            {
                "description": "my description1",
                "completion_date": utc_now,
            },
            {
                "description": "my description2",
                "completion_date": utc_now,
            },
        ],
    }

    # when
    response = await async_client.put(
        f"{BASE_ROUTE}/{current_user_goal_id}",
        json=request_body,
    )

    # then
    assert response.status_code == 200
    goal = response.json()
    expected_goal_schema = {
        "id": 1,
        "description": "my description",
        "status": "DONE",
        "title": "my title",
        "posts": [
            {
                "id": 1,
                "date": utc_now,
                "description": "current user post description",
                "number_of_comments_and_replies": 2,
                "number_of_likes": 0,
                "is_liked": False,
            }
        ],
        "steps": [
            {
                "id": 3,
                "completion_date": utc_now,
                "description": "my description1",
            },
            {
                "id": 4,
                "completion_date": utc_now,
                "description": "my description2",
            },
        ],
        "user": {
            "id": 1,
            "uid": "CurrentUserUid",
            "fullname": "current_user_firstname current_user_lastname",
            "profile_picture_url": "current_user_profile_picture_url",
        },
    }
    assert goal == expected_goal_schema
    expected_object = Goal(
        title="my title",
        description="my description",
        status=GoalStatus.DONE,
        creation_date=utc_now,
        user_id=1,
    )
    assert_model_objects_are_equal(await Goal.get(id=goal["id"]), expected_object)

    steps = await Step.filter(goal_id=goal["id"])
    assert len(steps) == 2
    expected_step_object1 = Step(
        description="my description1",
        completion_date=utc_now,
        goal_id=goal["id"],
    )
    expected_step_object2 = Step(
        description="my description2",
        completion_date=utc_now,
        goal_id=goal["id"],
    )
    assert_model_objects_are_equal(steps[0], expected_step_object1)
    assert_model_objects_are_equal(steps[1], expected_step_object2)


async def test_update_goal_withAccessForbidden(async_client: AsyncClient):
    # given
    other_user_goal_id = 2
    request_body = {
        "request_goal": {
            "description": "my description",
            "status": "DONE",
            "title": "title",
        }
    }

    # when
    response = await async_client.put(
        f"{BASE_ROUTE}/{other_user_goal_id}",
        json=request_body,
    )

    # then
    assert response.status_code == 403
    assert response.json() == {"detail": "Access forbidden"}


async def test_update_goal_withIdDoesNotExist(async_client: AsyncClient):
    # given
    unexisting_goal_id = 99
    request_body = {
        "request_goal": {
            "title": "title",
            "description": "finished",
            "status": "DONE",
        }
    }

    # when
    response = await async_client.put(
        f"{BASE_ROUTE}/{unexisting_goal_id}",
        json=request_body,
    )

    # then
    assert response.status_code == 404
    assert response.json() == {"detail": "Goal with id 99 does not exist"}


async def test_update_goal_withInvalidGoalStatus(async_client: AsyncClient):
    # given
    goal_id = 1
    request_body = {
        "request_goal": {
            "title": "title",
            "description": "my description",
            "status": "WRONG",
        }
    }

    # when
    response = await async_client.put(
        f"{BASE_ROUTE}/{goal_id}",
        json=request_body,
    )

    # then
    assert response.status_code == 422
    assert response.json() == {"detail": "WRONG is not a possible value for its field"}


async def test_update_goal_withEmptyTitle(async_client: AsyncClient):
    # given
    other_user_goal_id = 1
    request_body = {
        "request_goal": {
            "description": "my description",
            "status": "DONE",
            "title": "",
        }
    }

    # when
    response = await async_client.put(
        f"{BASE_ROUTE}/{other_user_goal_id}",
        json=request_body,
    )

    # then
    assert response.status_code == 422
    assert response.json() == {"detail": "title field can not be empty"}


async def test_delete_goal(async_client: AsyncClient):
    # given
    current_user_goal_id = 1

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/{current_user_goal_id}")

    # then
    assert response.status_code == 200
    assert response.json() == "Successfully deleted goal with id 1"

    assert not await Goal.exists(id=current_user_goal_id)


async def test_delete_goal_withAccessForbidden(async_client: AsyncClient):
    # given
    other_user_goal_id = 2

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/{other_user_goal_id}")

    # then
    assert response.status_code == 403
    assert response.json() == {"detail": "Access forbidden"}
    assert await Goal.exists(id=other_user_goal_id)


async def test_delete_goal_withIdDoesNotExist(async_client: AsyncClient):
    # given
    unexisting_goal_id = 99

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/{unexisting_goal_id}")

    # then
    assert response.status_code == 404
    assert response.json() == {"detail": "Goal with id 99 does not exist"}
