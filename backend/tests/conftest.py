from datetime import datetime, timezone

import pytest
from httpx import AsyncClient
from src.app_config import MODELS
from src.firebase import get_current_user
from src.main import app
from tests.database_preparation_fixtures import _get_current_user
from tortoise import Tortoise
from tortoise.models import Model

BASE_URL = "http://localhost:5000/api/v1"

pytest_plugins = ["tests.database_preparation_fixtures"]


@pytest.fixture(scope="function")
async def db_setup():
    await Tortoise.init(db_url="sqlite://:memory:", modules=MODELS)
    await Tortoise.generate_schemas()
    yield
    await Tortoise._drop_databases()
    await Tortoise.close_connections()


async def get_current_user_mock():
    return await _get_current_user()


@pytest.fixture
def mock_get_current_user():
    app.dependency_overrides[get_current_user] = get_current_user_mock


@pytest.fixture(scope="function")
async def async_client(
    db_setup,
    mock_get_current_user,
    prepare_current_user_data,
    prepare_other_user_data,
):
    async with AsyncClient(
        app=app,
        base_url=BASE_URL,
    ) as async_client:
        yield async_client


@pytest.fixture
async def freeze_time(freezer):
    yield datetime.now(timezone.utc).isoformat()


def assert_model_objects_are_equal(actual: Model, expected: Model):
    assert actual.id
    assert get_dict(actual) == get_dict(expected)


def get_dict(model: Model):
    model_dict = model.__dict__
    model_dict.pop("id", None)
    model_dict.pop("_partial", None)
    model_dict.pop("_saved_in_db", None)
    model_dict.pop("_custom_generated_pk", None)
    return model_dict
