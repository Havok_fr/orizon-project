import os

PORT: str = os.getenv("PORT")
bind = "0.0.0.0:" + PORT
workers = 1
worker_class = "uvicorn.workers.UvicornWorker"
reload = False
