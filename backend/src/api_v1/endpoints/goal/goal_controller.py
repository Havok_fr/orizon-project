from fastapi import APIRouter, Depends
from loguru import logger
from src.api_v1.endpoints.goal import goal_service
from src.api_v1.http_constantes import (
    HTTP_NOT_FOUND,
    HTTP_NOT_FOUND_AND_UNPROCESSABLE_ENTITY,
)
from src.data_validation.enum import assert_that_value_is_in_enum
from src.firebase import get_current_user
from src.models import GoalStatus, User
from src.schemas import (
    RequestStepWithoutGoalId,
    ResponseGoal,
    RequestGoal,
    ResponseGoalSlim,
)

router = APIRouter()


@router.get(
    path="/",
    response_model=list[ResponseGoalSlim],
)
async def get_all_goals(
    offset: int = 0,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Get goals with offset {}",
        current_user.id,
        offset,
    )
    response_goals = await goal_service.get_all(
        offset=offset,
        current_user=current_user,
    )
    logger.info("Goals({})", response_goals)
    return response_goals


@router.get(
    path="/{goal_id}",
    response_model=ResponseGoal,
    responses=HTTP_NOT_FOUND,
)
async def get_goal(
    goal_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Get goal with id {}",
        current_user.id,
        goal_id,
    )
    response_goal = await goal_service.get_by_id(
        goal_id=goal_id,
        current_user=current_user,
    )
    logger.info("Goal({})", response_goal)
    return response_goal


@router.post(
    path="/",
    response_model=ResponseGoalSlim,
    responses=HTTP_NOT_FOUND_AND_UNPROCESSABLE_ENTITY,
)
async def create_goal(
    request_goal: RequestGoal,
    request_steps: list[RequestStepWithoutGoalId] = [],
    current_user: User = Depends(get_current_user),
):
    assert_that_value_is_in_enum(request_goal.status, GoalStatus)
    logger.info(
        "[Current User ID: {}] Create Goal({}) with Steps ({})",
        current_user.id,
        request_goal,
        request_steps,
    )
    return await goal_service.create(
        request_goal=request_goal,
        request_steps=request_steps,
        current_user=current_user,
    )


@router.put(
    path="/{goal_id}",
    response_model=ResponseGoal,
    responses=HTTP_NOT_FOUND,
)
async def update_goal(
    goal_id: int,
    request_goal: RequestGoal,
    request_steps: list[RequestStepWithoutGoalId] = [],
    current_user: User = Depends(get_current_user),
):
    assert_that_value_is_in_enum(request_goal.status, GoalStatus)
    logger.info(
        "[Current User ID: {}] Update goal with id {}. New values: Goal({}) Steps({})",
        current_user.id,
        goal_id,
        request_goal,
        request_steps,
    )
    return await goal_service.update(
        goal_id=goal_id,
        request_goal=request_goal,
        request_steps=request_steps,
        current_user=current_user,
    )


@router.delete(
    path="/{goal_id}",
    responses=HTTP_NOT_FOUND,
)
async def delete_goal(
    goal_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Delete goal with id {}",
        current_user.id,
        goal_id,
    )
    await goal_service.delete(
        goal_id=goal_id,
        current_user=current_user,
    )
    return f"Successfully deleted goal with id {goal_id}"


@router.get(
    path="/search/{goal_title}",
    response_model=list[ResponseGoalSlim],
)
async def search_goal(
    goal_title: str,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] search goals with name",
        current_user.id,
        goal_title,
    )
    response_goals = await goal_service.search(
        goal_title=goal_title,
        current_user=current_user,
    )
    logger.info("Goals({})", response_goals)
    return response_goals
