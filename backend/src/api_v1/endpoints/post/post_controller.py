from fastapi import APIRouter, Depends
from loguru import logger
from src.api_v1.endpoints.post import post_service
from src.api_v1.http_constantes import (
    HTTP_NOT_FOUND,
    HTTP_NOT_FOUND_AND_UNPROCESSABLE_ENTITY,
)
from src.firebase import get_current_user
from src.models import User
from src.schemas import ResponsePost, RequestPost


router = APIRouter()


@router.get(
    path="/",
    response_model=list[ResponsePost],
)
async def get_feed_posts(
    offset: int = 0,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Get feed posts with offset {}",
        current_user.id,
        offset,
    )
    response_posts = await post_service.get_feed(
        offset=offset,
        current_user=current_user,
    )
    logger.info("Posts({})", response_posts)
    return response_posts


@router.post(
    path="/",
    response_model=ResponsePost,
    responses=HTTP_NOT_FOUND_AND_UNPROCESSABLE_ENTITY,
)
async def create_post(
    request_post: RequestPost,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Create Post({})",
        current_user.id,
        request_post,
    )
    return await post_service.create(
        request_post=request_post,
        current_user=current_user,
    )


@router.delete(
    path="/{post_id}",
    responses=HTTP_NOT_FOUND,
)
async def delete_post(
    post_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Delete post with id {}",
        current_user.id,
        post_id,
    )
    await post_service.delete(
        post_id=post_id,
        current_user=current_user,
    )
    return f"Successfully deleted post with id {post_id}"


@router.post(
    path="/{post_id}/like",
    responses=HTTP_NOT_FOUND,
)
async def like_post(
    post_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Like post with id {}",
        current_user.id,
        post_id,
    )
    await post_service.like(
        post_id=post_id,
        current_user=current_user,
    )
    return f"Successfully liked post with id {post_id}"


@router.post(
    path="/{post_id}/unlike",
    responses=HTTP_NOT_FOUND,
)
async def unlike_post(
    post_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Unlike post with id {}",
        current_user.id,
        post_id,
    )
    await post_service.unlike(
        post_id=post_id,
        current_user=current_user,
    )
    return f"Successfully unliked post with id {post_id}"


@router.get(
    path="/{post_id}/supporters",
    responses=HTTP_NOT_FOUND,
)
async def get_supporters(
    post_id: int,
    offset: int = 0,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Get supporters of post with id {} and offset {}",
        current_user.id,
        post_id,
        offset,
    )
    supporters = await post_service.get_supporters(
        offset=offset,
        post_id=post_id,
        current_user=current_user,
    )
    logger.info("Supporters({})", supporters)
    return supporters
