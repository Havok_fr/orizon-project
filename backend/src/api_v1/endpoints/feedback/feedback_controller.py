from fastapi import APIRouter, Depends, BackgroundTasks
from loguru import logger
from src.api_v1.endpoints.feedback import feedback_service
from src.firebase import get_current_user
from src.models import User
from src.schemas import RequestFeedback

router = APIRouter()


@router.post(
    path="/",
)
async def send_feedback(
    background_tasks: BackgroundTasks,
    request_feedback: RequestFeedback,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Send feeback: {}",
        current_user.id,
        request_feedback,
    )
    background_tasks.add_task(
        feedback_service.send,
        request_feedback=request_feedback,
        current_user=current_user,
    )
    return "Feedback successfully sent"
