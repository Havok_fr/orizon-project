from src.models import User
from src.app_config import (
    EMAIL_RECEIVER_EMAIL,
    EMAIL_SENDER_EMAIL,
    EMAIL_SENDER_PASSWORD,
)
from email.message import EmailMessage

from src.schemas import RequestFeedback

import smtplib


async def send(
    request_feedback: RequestFeedback,
    current_user: User,
):
    email = _build_email(request_feedback, current_user.email)
    _send_email(email)


def _build_email(
    request_feedback: RequestFeedback,
    current_user_email: str,
) -> EmailMessage:
    email = EmailMessage()
    email_subject = f"From {current_user_email}, {request_feedback.category} : {request_feedback.title}"
    email["Subject"] = email_subject
    email.set_content(request_feedback.message)
    return email


def _send_email(email: EmailMessage) -> None:
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.starttls()
    server.login(EMAIL_SENDER_EMAIL, EMAIL_SENDER_PASSWORD)
    server.send_message(email, EMAIL_SENDER_EMAIL, EMAIL_RECEIVER_EMAIL)
    server.close()
