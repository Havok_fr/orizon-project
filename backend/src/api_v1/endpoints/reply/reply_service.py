from src.data_access import (
    assert_that_current_user_is_reply_owner,
    assert_that_post_is_accessible,
)
from src.data_validation.id import (
    assert_that_comment_exists,
    assert_that_reply_exists,
)
from src.models import Comment, Reply, User
from src.schemas import RequestCreateReply, ResponseReply


async def create(
    request_reply: RequestCreateReply,
    current_user: User,
) -> ResponseReply:
    await assert_that_comment_exists(request_reply.comment_id)
    comment = await Comment.get(id=request_reply.comment_id)
    await assert_that_post_is_accessible(
        post_id=comment.post_id,
        current_user=current_user,
    )
    return await ResponseReply.from_tortoise_orm(
        await Reply.create(
            **request_reply.dict(exclude_unset=True),
            writer_id=current_user.id,
        )
    )


async def delete(
    reply_id: int,
    current_user: User,
) -> None:
    await assert_that_reply_exists(reply_id)
    await assert_that_current_user_is_reply_owner(
        reply_id=reply_id,
        current_user_id=current_user.id,
    )
    await Reply.filter(id=reply_id).delete()
