from fastapi import APIRouter, Depends
from loguru import logger
from src.api_v1.endpoints.reply import reply_service
from src.api_v1.http_constantes import HTTP_NOT_FOUND
from src.firebase import get_current_user
from src.models import User
from src.schemas import RequestCreateReply, ResponseReply

router = APIRouter()


@router.post(
    path="/",
    response_model=ResponseReply,
    responses=HTTP_NOT_FOUND,
)
async def create_reply(
    request_reply: RequestCreateReply,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Create Reply({})",
        current_user.id,
        request_reply,
    )
    return await reply_service.create(
        request_reply=request_reply,
        current_user=current_user,
    )


@router.delete(
    path="/{reply_id}",
    responses=HTTP_NOT_FOUND,
)
async def delete_reply(
    reply_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Delete reply with id {}",
        current_user.id,
        reply_id,
    )
    await reply_service.delete(
        reply_id=reply_id,
        current_user=current_user,
    )
    return f"Successfully deleted reply with id {reply_id}"
