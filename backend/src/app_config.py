import os

# FastApi
PROJECT_NAME = "Orizon project"
ALLOW_CREDENTIALS = False
ALLOWED_ORIGINS = [os.getenv("ALLOWED_ORIGIN")]
ALLOWED_METHODS = ["OPTIONS", "GET", "POST", "PUT", "DELETE"]
ALLOWED_HEADERS = [
    "Accept",
    "Accept-Language",
    "Content-Language",
    "Content-Type",
    "Authorization",
]
API_PATH_PREFIX = "/api/v1"
OPENAPI_URL = os.getenv("OPENAPI_URL")
DOCS_URL = os.getenv("DOCS_URL")
SWAGGER_UI_OAUTH2_REDIRECT_URL = None
REDOC_URL = None

# Tortoise ORM
DATABASE_URL = os.getenv("DATABASE_URL")
GENERATE_SCHEMAS = False
ADD_EXCEPTION_HANDLERS = False
MODELS_FILE_PATH = ["src.models", "aerich.models"]
MODELS = {"models": MODELS_FILE_PATH}
TORTOISE_ORM_CONFIG = {
    "connections": {"default": DATABASE_URL},
    "apps": {"models": MODELS},
}

# Firebase
FIREBASE_CREDENTIALS_JSON = os.getenv("FIREBASE_CREDENTIALS_JSON")

# Feedback
EMAIL_SENDER_EMAIL = os.getenv("EMAIL_SENDER_EMAIL")
EMAIL_RECEIVER_EMAIL = os.getenv("EMAIL_RECEIVER_EMAIL")
EMAIL_SENDER_PASSWORD = os.getenv("EMAIL_SENDER_PASSWORD")
