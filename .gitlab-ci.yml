variables:
  DOCKER_DRIVER: overlay
  BACKEND_DIR_PATH: backend
  FRONTENT_DIR_PATH: frontend
  DOCKER_DIR_PATH: docker
  HEROKU_BACKEND_APP_NAME: orizon-backend
  HEROKU_FRONTEND_APP_NAME: orizon-frontend
  HEROKU_REGISTRY: registry.heroku.com

stages:
  - tests
  - build
  - deploy

tests::backend:
  stage: tests
  image: registry.gitlab.com/orizon-group/base-python-image:latest
  before_script:
    - cd $BACKEND_DIR_PATH
    - poetry config virtualenvs.in-project true
    - poetry install
  script:
    - poetry run pylint -d C0301,C0114,C0116 *.py
    - poetry run pytest -v --cov=src --cov-report=term-missing tests
  cache:
    paths:
      - $BACKEND_DIR_PATH/.venv
  only:
    - merge_requests
    - tags

tests::frontend:
  image: node:11.12.0-alpine
  stage: tests
  before_script:
    - cd $FRONTENT_DIR_PATH
    - npm ci --cache .npm-cache
    - npm install --progress=false
  script:
    - npm run lint
    - npm run test
  cache:
    paths:
      - $FRONTENT_DIR_PATH/.npm-cache
  only:
    - merge_requests
    - tags

build::backend:
  image: docker:stable
  services:
    - docker:dind
  stage: build
  needs:
    - tests::backend
  before_script:
    - >
      if [ -z "$CI_COMMIT_TAG"]; then
        BUILD_VERSION="dev"
      else
        BUILD_VERSION="$CI_COMMIT_TAG"
      fi
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:latest -t $CI_REGISTRY_IMAGE:latest -f $DOCKER_DIR_PATH/Dockerfile .
    - docker build -t $CI_REGISTRY_IMAGE:latest -f $DOCKER_DIR_PATH/Dockerfile .
    - docker tag $CI_REGISTRY_IMAGE:latest $CI_REGISTRY_IMAGE:$BUILD_VERSION
    - docker push $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:$BUILD_VERSION
  only:
    - merge_requests
    - tags

build::frontend::preprod:
  variables:
    VUE_APP_FIREBASE_CREDENTIALS_JSON: $VUE_APP_FIREBASE_CREDENTIALS_JSON_PREPROD
    VUE_APP_BACKEND_URL: //${HEROKU_BACKEND_APP_NAME}-preprod.herokuapp.com/api/v1
  image: node:11.12.0-alpine
  stage: build
  needs:
    - tests::frontend
  before_script:
    - cd $FRONTENT_DIR_PATH
    - npm ci --cache .npm-cache
    - npm install --progress=false
  script:
    - npm run build
  cache:
    paths:
      - $FRONTENT_DIR_PATH/.npm-cache
  artifacts:
    expire_in: 1 week
    paths:
      - $FRONTENT_DIR_PATH/dist
  only:
    - merge_requests
    - tags

build::frontend::prod:
  variables:
    VUE_APP_FIREBASE_CREDENTIALS_JSON: $VUE_APP_FIREBASE_CREDENTIALS_JSON_PROD
    VUE_APP_BACKEND_URL: //${HEROKU_BACKEND_APP_NAME}-prod.herokuapp.com/api/v1  
  image: node:11.12.0-alpine
  stage: build
  needs:
    - tests::frontend
  before_script:
    - cd $FRONTENT_DIR_PATH
     - npm ci --cache .npm-cache
    - npm install --progress=false
  script:
    - npm run build
  cache:
    paths:
      - $FRONTENT_DIR_PATH/.npm-cache
  artifacts:
    expire_in: 1 week
    paths:
      - $FRONTENT_DIR_PATH/dist
  only:
    - tags
  when: manual

deploy::backend::preprod:
  variables:
    ENV: preprod
    HEROKU_REGISTRY_IMAGE: ${HEROKU_REGISTRY}/${HEROKU_BACKEND_APP_NAME}-${ENV}/web
  image: docker:stable
  services:
    - docker:dind
  stage: deploy
  dependencies: []
  needs:
    - build::frontend::preprod
    - build::backend
  before_script:
    - apk add --no-cache curl
    - chmod +x ./deploy_backend.sh
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG $HEROKU_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - docker tag $HEROKU_REGISTRY_IMAGE:$CI_COMMIT_TAG $HEROKU_REGISTRY_IMAGE:latest
    - docker login -u _ -p $HEROKU_AUTH_TOKEN $HEROKU_REGISTRY
    - docker push $HEROKU_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - docker push $HEROKU_REGISTRY_IMAGE:latest
    - ./deploy_backend.sh
  only:
    - tags

deploy::backend::prod:
  variables:
    ENV: prod
    HEROKU_REGISTRY_IMAGE: ${HEROKU_REGISTRY}/${HEROKU_BACKEND_APP_NAME}-${ENV}/web
  image: docker:stable
  services:
    - docker:dind
  stage: deploy
  dependencies: []
  needs:
    - build::frontend::prod
    - build::backend
  before_script:
    - apk add --no-cache curl
    - chmod +x ./deploy_backend.sh
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG $HEROKU_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - docker tag $HEROKU_REGISTRY_IMAGE:$CI_COMMIT_TAG $HEROKU_REGISTRY_IMAGE:latest
    - docker login -u _ -p $HEROKU_AUTH_TOKEN $HEROKU_REGISTRY
    - docker push $HEROKU_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - docker push $HEROKU_REGISTRY_IMAGE:latest
    - ./deploy_backend.sh
  only:
    - tags
  when: manual

deploy::frontend::preprod:
  image: tmaier/dpl
  stage: deploy
  needs:
    - build::frontend::preprod
    - build::backend
  script:
    - cd $FRONTENT_DIR_PATH/dist
    - touch .static
    - dpl --provider=heroku --app=${HEROKU_FRONTEND_APP_NAME}-preprod --api-key=$HEROKU_AUTH_TOKEN --skip_cleanup
  only:
    - tags

deploy::frontend::prod:
  image: tmaier/dpl
  stage: deploy
  needs:
    - build::frontend::prod
    - build::backend
  script:
    - cd $FRONTENT_DIR_PATH/dist
    - touch .static
    - dpl --provider=heroku --app=${HEROKU_FRONTEND_APP_NAME}-prod --api-key=$HEROKU_AUTH_TOKEN --skip_cleanup
  only:
    - tags
  when: manual

