import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import TimeAgo from "javascript-time-ago";
import firebaseAuthService from "./services/firebase-auth";

import en from "javascript-time-ago/locale/en";
import fr from "javascript-time-ago/locale/fr";

import { IonicVue } from "@ionic/vue";
import { createI18n } from "vue-i18n";

import "@ionic/vue/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/vue/css/normalize.css";
import "@ionic/vue/css/structure.css";
import "@ionic/vue/css/typography.css";

import "./assets/css/tailwind.min.css";

import "./assets/css/global.css";

/* Optional CSS utils that can be commented out */
import "@ionic/vue/css/padding.css";
import "@ionic/vue/css/float-elements.css";
import "@ionic/vue/css/text-alignment.css";
import "@ionic/vue/css/text-transformation.css";
import "@ionic/vue/css/flex-utils.css";
import "@ionic/vue/css/display.css";

// language related imports
import enConstants from "./translations/en.json";
import frConstants from "./translations/fr.json";

/* Theme variables */
import "./theme/ionic-colors.css";

import "./registerServiceWorker";

TimeAgo.addDefaultLocale(en);
TimeAgo.addLocale(fr);

const i18n = createI18n({
  messages: {
    fr: frConstants,
    en: enConstants,
  },
  fallbackLocale: "en",
});

const app = createApp(App)
  .use(i18n)
  .use(IonicVue)
  .use(store)
  .use(router);

app.config.globalProperties.$appName = "Orizon";

const timeAgo = new TimeAgo(store.state.appLang);

app.config.globalProperties.$timeAgo = timeAgo;

document.body.classList.toggle("dark", store.state.isInDarkMode);

firebaseAuthService.setUp();

router.isReady().then(() => {
  app.mount("#app");
});
