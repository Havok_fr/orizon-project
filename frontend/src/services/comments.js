import getBackendApi from "./backend-api";

const BASE_ROUTE = "comments";

export default {
  async getAll(postId, offset = 0) {
    return (await getBackendApi()).get(
      BASE_ROUTE + "/post/" + postId,
      { params: { offset: offset } }
    );
  },
  async create(content, postId) {
    const comment = {
      content: content,
      post_id: postId,
    };
    return (await getBackendApi()).post(BASE_ROUTE, comment);
  },
  async delete(commentId) {
    return (await getBackendApi()).delete(BASE_ROUTE + "/" + commentId);
  },
};
