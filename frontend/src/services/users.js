import getBackendApi from "./backend-api";

const BASE_ROUTE = "users";

export default {
  async get(userId) {
    return (await getBackendApi()).get(BASE_ROUTE + "/" + userId);
  },
  async follow(userId) {
    return (await getBackendApi()).post(BASE_ROUTE + "/" + userId + "/follow");
  },
  async cancelFollow(userId) {
    return (await getBackendApi()).post(BASE_ROUTE + "/" + userId + "/cancel_follow");
  },
  async unfollow(userId) {
    return (await getBackendApi()).post(BASE_ROUTE + "/" + userId + "/unfollow");
  },
  async acceptFollow(requesterId) {
    return (await getBackendApi()).post(BASE_ROUTE + "/" + requesterId + "/accept_follow");
  },
  async denyFollow(requesterId) {
    return (await getBackendApi()).post(BASE_ROUTE + "/" + requesterId + "/deny_follow");
  },
  async removeFollow(followerId) {
    return (await getBackendApi()).put(BASE_ROUTE + "/" + followerId + "/remove_follow");
  },
  async me() {
    return (await getBackendApi()).get(BASE_ROUTE + "/me");
  },
  async getFollowers(userId, offset = 0) {
    return (await getBackendApi()).get(
      BASE_ROUTE + "/" + userId + "/followers",
      { params: { offset: offset } }
    );
  },
  async getFollowing(userId, offset = 0) {
    return (await getBackendApi()).get(
      BASE_ROUTE + "/" + userId + "/following",
      { params: { offset: offset } }
    );
  },
  async switchToPublic() {
    return (await getBackendApi()).put(BASE_ROUTE + "/me/public");
  },
  async switchToPrivate() {
    return (await getBackendApi()).put(BASE_ROUTE + "/me/private");
  },
  async search(username, offset = 0) {
    return (await getBackendApi()).get(
      BASE_ROUTE + "/search/" + username,
      { params: { offset: offset } }
    );
  },
  async deleteAccount() {
    return (await getBackendApi()).delete(BASE_ROUTE + "/me");
  }
};
