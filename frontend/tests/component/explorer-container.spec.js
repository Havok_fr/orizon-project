import { mount } from "@vue/test-utils";
import ExploreContainer from "../../src/components/ExploreContainer";

// given the component which contains text
// when we init the component
// then we check contained text

describe("ExplorerContainer.vue", () => {
  it("renders text", () => {
    const wrapper = mount(ExploreContainer);
    expect(wrapper.text()).toMatch("Explore UI Components");
  });
});
